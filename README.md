# Story Files of PPW 2018

## Overview
Proyek CrowdFunding untuk memenuhi tugas PPW

## Application Status
[![pipeline status](https://gitlab.com/Ervan.Adiwijaya/PPW2018_Lab/badges/master/pipeline.svg)](https://gitlab.com/Ervan.Adiwijaya/PPW2018_Lab/commits/master)
[![coverage report](https://gitlab.com/Ervan.Adiwijaya/PPW2018_Lab/badges/master/coverage.svg)](https://gitlab.com/Ervan.Adiwijaya/PPW2018_Lab/commits/master)

## Link Heroku Application
[https://ppwa-pepewewe.herokuapp.com/](https://ppwa-pepewewe.herokuapp.com/)