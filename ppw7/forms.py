from django import forms

class Update_Status (forms.Form) :
    error_messages = {
        'invalid' : 'Mohon diisi sebelum men-submit',
    }
    attrs = {
        'class' : 'form-control',
        'id' : 'status_bar',
    }

    status = forms.CharField(label = 'status', required = True,
     widget = forms.TextInput(attrs=attrs))