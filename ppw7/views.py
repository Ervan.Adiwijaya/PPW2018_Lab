from .forms import Update_Status
from .models import Status
from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

response = {}

'''def index2 (request) :
    return render(request,'base.html',{'status_update' : Update_Status, 
    'status_list' : Status.objects.all()})'''

def index (request) :
    form = Update_Status(request.POST or None)
    if (request.method == 'POST' and form.is_valid()) :
        response['status'] = request.POST['status'] if request.POST['status'] != "" else "Mohon jangan update status kosong"

        submit_status = Status(status = response['status'])
        submit_status.save()
        return HttpResponseRedirect('/done/')
    else :
        return render(request,'base.html',{'status_list' : Status.objects.all(),
        'status_update' : Update_Status})

def redirect_view(request) :
    return HttpResponseRedirect('/ppw7/')

def biodata_view(request) :
    return render(request, 'biodata.html')