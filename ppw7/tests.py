# from django.test import TestCase, Client, LiveServerTestCase
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import index, redirect_view, biodata_view
# from .models import Status

# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options

# # Create your tests here.
# class ppw7Test (TestCase) :
#     def test_ppw7_exist(self) :
#         response = Client().get('/ppw7/')
#         self.assertEqual(response.status_code,200)

#     def test_ppw7_biodata_page_exists(self) :
#         response = Client().get('/biodata/')
#         self.assertEqual(response.status_code, 200)


#     def test_ppw7_use_base_template(self) :
#         response = Client().get('/ppw7/')
#         self.assertTemplateUsed(response,'base.html')

#     def test_ppw7_biodata_html_used(self) :
#         response = Client().get('/biodata/')
#         self.assertTemplateUsed(response, 'biodata.html')
    
#     def test_ppw7_is_text_can_be_viewed_on_bio(self) :
#         request = HttpRequest()
#         response = biodata_view(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('Ervan Adiwijaya Haryadi',html_response)

#     def test_ppw7_is_text_can_be_viewed_on_index(self) :
#         request = HttpRequest()
#         response = index(request)
#         html_response = response.content.decode('utf8')
#         self.assertIn('Hello, Apa Kabar?',html_response)

#     def test_ppw7_use_index_func(self) :
#         found = resolve('/ppw7/')
#         self.assertEqual(found.func, index)

#     def test_ppw7_biodata_func(self) :
#         found = resolve('/biodata/')
#         self.assertEqual(found.func, biodata_view)
    
#     def test_ppw7_use_redirect_func(self) :
#         found = Client().get('/')
#         self.assertRedirects(found,'/ppw7/')

#     def test_ppw7_models_string_representation(self) :
#         newQuery = Status(status="hi")
#         self.assertEqual(str(newQuery),newQuery.status)

#     def test_ppw7_can_save_POST_request(self) :
#         response = self.client.post('/ppw7/', data = {'status' : 'testing status 1'})
#         counting_all_variables = Status.objects.all().count()
#         self.assertEqual(counting_all_variables, 1)
#         self.assertEqual(response.status_code, 302)
#         self.assertEqual(response['location'], '/done/')

# class ppw7FuncTest(LiveServerTestCase) :

#     def setUp(self) :
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--dns-prefetch-disable')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium = webdriver.Chrome(chrome_options=options)
#         self.selenium.implicitly_wait(25)
#         super(ppw7FuncTest, self).setUp()

#     def tearDown(self) :
#         self.selenium.quit()
#         super(ppw7FuncTest, self).tearDown()

# #tes lokal
#     def test_toDo_form_submit_test(self) :
#         selenium = self.selenium
#         selenium.get("http://127.0.0.1:8000/")#self.live_server_url)

#         status = selenium.find_element_by_id('status_bar')
#         submit = selenium.find_element_by_id('submit')
        
#         status.send_keys("Coba Coba")
#         submit.send_keys(Keys.RETURN)
#         self.assertIn("Coba Coba", selenium.page_source)

#     def test_toDo_element_test1(self) :
#         selenium = self.selenium
#         selenium.get("http://127.0.0.1:8000/")

#         judul = selenium.find_element_by_id("judul").text
#         self.assertIn("Hello, Apa Kabar?",judul)

#     def test_toDo_element_test2(self) :
#         selenium = self.selenium
#         selenium.get("http://127.0.0.1:8000/")

#         sub_judul = selenium.find_element_by_id("sub_judul").text
#         self.assertIn("Status Updates",sub_judul)

#     def test_toDo_css_test1(self) :
#         selenium = self.selenium
#         selenium.get("http://127.0.0.1:8000/")

#         judul = selenium.find_element_by_class_name("judul").value_of_css_property('color')
#         self.assertIn("rgba(0, 0, 255, 1)",judul)

#     def test_toDo_css_test2(self) :
#         selenium = self.selenium
#         selenium.get("http://127.0.0.1:8000/")

#         subjudul = selenium.find_element_by_class_name("subjudul").value_of_css_property('color')
#         self.assertIn("rgba(102, 51, 153, 1)",subjudul)


# #tes heroku
#     def test_heroku_toDo_form_submit_test(self) :
#         selenium = self.selenium
#         selenium.get("http://ppwa-pepewewe.herokuapp.com/ppw7/")

#         status = selenium.find_element_by_id('status_bar')
#         submit = selenium.find_element_by_id('submit')
        
#         status.send_keys("Coba Coba")
#         submit.send_keys(Keys.RETURN)
#         self.assertIn("Coba Coba", selenium.page_source)

#     def test_heroku_toDo_element_test1(self) :
#         selenium = self.selenium
#         selenium.get("http://ppwa-pepewewe.herokuapp.com/ppw7/")

#         judul = selenium.find_element_by_id("judul").text
#         self.assertIn("Hello, Apa Kabar?",judul)

#     def test_heroku_toDo_element_test2(self) :
#         selenium = self.selenium
#         selenium.get("http://ppwa-pepewewe.herokuapp.com/ppw7/")

#         sub_judul = selenium.find_element_by_tag_name("h3").text
#         self.assertIn("Status Updates",sub_judul)

#     def test_heroku_toDo_css_test1(self) :
#         selenium = self.selenium
#         selenium.get("http://ppwa-pepewewe.herokuapp.com/ppw7/")

#         judul = selenium.find_element_by_class_name("column").value_of_css_property('float')
#         self.assertIn("left",judul)

#     def test_heroku_toDo_css_test2(self) :
#         selenium = self.selenium
#         selenium.get("http://ppwa-pepewewe.herokuapp.com/ppw7/")

#         judul = selenium.find_element_by_class_name("column").value_of_css_property('padding')
#         self.assertIn("10px",judul)