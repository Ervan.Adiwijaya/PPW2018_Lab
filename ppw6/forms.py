from django import forms

class Update_Status (forms.Form) :
    error_messages = {
        'invalid' : 'Mohon diisi sebelum men-submit',
    }
    attrs = {
        'class' : 'form-control'
    }

    status = forms.CharField(label = 'status', max_length='300', required = True,
     widget = forms.TextInput(attrs=attrs))