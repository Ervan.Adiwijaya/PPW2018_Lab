from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, redirect_view, biodata_view
from .models import Status

# Create your tests here.
class PPW6Test (TestCase) :
    def test_ppw6_exist(self) :
        response = Client().get('/ppw6/')
        self.assertEqual(response.status_code,200)

    def test_ppw6_biodata_page_exists(self) :
        response = Client().get('/biodata/')
        self.assertEqual(response.status_code, 200)


    def test_ppw6_use_base_template(self) :
        response = Client().get('/ppw6/')
        self.assertTemplateUsed(response,'base.html')

    def test_ppw6_biodata_html_used(self) :
        response = Client().get('/biodata/')
        self.assertTemplateUsed(response, 'biodata.html')
    
    def test_ppw6_is_text_can_be_viewed_on_bio(self) :
        request = HttpRequest()
        response = biodata_view(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Ervan Adiwijaya Haryadi',html_response)

    def test_ppw6_is_text_can_be_viewed_on_index(self) :
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa Kabar?',html_response)

    def test_ppw6_use_index_func(self) :
        found = resolve('/ppw6/')
        self.assertEqual(found.func, index)

    def test_ppw6_biodata_func(self) :
        found = resolve('/biodata/')
        self.assertEqual(found.func, biodata_view)
    
    def test_ppw6_use_redirect_func(self) :
        found = Client().get('/')
        self.assertRedirects(found,'/ppw6/')

    def test_ppw6_models_string_representation(self) :
        newQuery = Status(status="hi")
        self.assertEqual(str(newQuery),newQuery.status)

    def test_ppw6_can_save_POST_request(self) :
        response = self.client.post('/ppw6/', data = {'status' : 'testing status 1'})
        counting_all_variables = Status.objects.all().count()
        self.assertEqual(counting_all_variables, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/done/')

    def test_ppw6_is_accordion_can_be_viewed_on_index(self) :
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Activities',html_response)