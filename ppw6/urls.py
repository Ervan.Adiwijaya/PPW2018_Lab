from django.urls import include, path, re_path
from django.contrib import admin
from .views import *

urlpatterns = [
    re_path(r'^ppw6/', index, name = 'index'),
    re_path(r'^done/',redirect_view, name = 'update_status'),
    re_path(r'^biodata/',biodata_view, name='biodata'),
    re_path(r'^', redirect_view, name = 'redirect'),
]