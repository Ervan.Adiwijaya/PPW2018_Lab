from django import forms
import datetime


class Form_Kegiatan(forms.Form):
    error_messages = {
        'required': 'Wajib diisi',
        'invalid': 'Input salah, mohon diulangi',
    }
    attrs = {
        'class': 'form-control'
    }
    attrs_date = {
        'class': 'form-control',
        'type': 'date'
    }
    attrs_time = {
        'class': 'form-control',
        'type': 'time'
    }

    tanggal = forms.DateField(label='tanggal', required=True,
                              widget=forms.DateInput(attrs=attrs_date))
    jam = forms.TimeField(label='jam', required=True,
                              widget=forms.TimeInput(attrs=attrs_time))
    kegiatan = forms.CharField(label='kegiatan', required=True,
                               widget=forms.TextInput(attrs=attrs))
    tempat = forms.CharField(label='tempat', required=False,
                            widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label='kategori', required=False,
                               widget=forms.TextInput(attrs=attrs))
