from django.db import models
# Create your models here.

class Jadwal(models.Model):
    tanggal = models.DateField()
    jam = models.TimeField()
    kegiatan = models.CharField(max_length = 200)
    tempat = models.CharField(max_length = 200)
    kategori = models.CharField(max_length = 200)

    def __str__(self):
        return self.kegiatan