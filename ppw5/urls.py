from django.urls import include, path, re_path
from django.contrib import admin
from .views import *

urlpatterns = [
    re_path (r'^jadwal/', jadwal, name = 'jadwal'),
    re_path (r'^hasil/', reg_post, name = 'submit_jadwal'),
    re_path (r'^hapus/', hapus_jadwal, name = 'delete_jadwal'),
    re_path (r'^',regis, name = 'registrasi'),
]