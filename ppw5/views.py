from django.shortcuts import render
from .forms import Form_Kegiatan
from .models import Jadwal
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def regis (request):
    return render(request, 'form.html',
    {'form_kegiatan' : Form_Kegiatan})

def jadwal (request):
    return render(request,'jadwal.html',
    {'jadwal' : Jadwal.objects.all()} )

def hapus_jadwal(request):
    return render(request,'jadwal.html', {'jadwal' : Jadwal.objects.all().delete()} )

def reg_post (request):
    form = Form_Kegiatan(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['tanggal'] = request.POST['tanggal'] if request.POST['tanggal'] != "" else "tanggal salah"
        response['jam'] = request.POST['jam'] if request.POST['jam'] != "" else "waktu salah"
        response['kegiatan'] = request.POST['kegiatan'] if request.POST['kegiatan'] != "" else "kegiatan salah"
        response['tempat'] = request.POST['tempat'] if request.POST['tempat'] != "" else "tempat salah"
        response['kategori'] = request.POST['kategori'] if request.POST['kategori'] != "" else "kategori salah"
        
        jadwal_kegiatan = Jadwal(tanggal = response['tanggal'],jam = response['jam'],
                                 kegiatan = response['kegiatan'],tempat = response['tempat'],
                                 kategori = response['kategori'])
        jadwal_kegiatan.save()
        return HttpResponseRedirect('/jadwal/')
    else :
        return HttpResponseRedirect('/home/')