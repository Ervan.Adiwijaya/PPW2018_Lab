# Generated by Django 2.1.1 on 2018-10-03 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField()),
                ('kegiatan', models.CharField(max_length=200)),
                ('tempat', models.CharField(max_length=200)),
                ('kategori', models.CharField(max_length=200)),
            ],
        ),
    ]
