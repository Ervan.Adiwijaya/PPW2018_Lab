$(document).ready(function($){
        
    var panels = $('.accordion > dd').hide();
    panels.first().show();
    $('.accordion > dt > a').click(function() {
    var $this = $(this);
    panels.slideUp();
    $this.parent().next().slideDown();
    return false;
    });

    // Hide all panels to start
    // Show first panel on load (optional). Remove if you want
    // On click of panel title
    // Slide up all other panels
    //Slide down target panel

    var check = 1;
    $("#changetheme").click(function(){
      if (check == 1){
        whiteTheme(); check = 0;
      } else {
        greenTheme(); check = 1;
      }
    })


    function greenTheme() {
        $('#changetheme').html('Green<br>Theme');
        $('.content').css({
          'background-color' : "#32CD32"
        })

        $('#changetheme').css({
            'background-color' : "#32CD32"
          })
    }

    function whiteTheme() {
        $('#changetheme').html('White<br>Theme');
        $('.content').css({
          'background-color' : '#FFFFFF'
        })

        $('#changetheme').css({
            'background-color' : "#FFFFFF"
          })
    }

});